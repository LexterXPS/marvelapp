export default async function AjaxRequests (url, method = 'GET') {
  try {
    const response = await fetch(url, {
      method: method,
      body: null
    })
    return response.json()
  } catch (error) {
    console.log(error)
  }
}

import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loading: false,
    characters: [],
    favorites_list: {
      characters: [],
      comics: [],
      stories: []
    },
    ComicsPerCharacter: [],
    StoriesPerCharacter: [],
    filteredCharacters: [],
    setCharactersPerComic: [],
    comicsPerStory: [],
    charactersPerStory: [],
    storiesPerComic: [],
    noResults: false
  },
  mutations: {
    setCharacters (state, payload) {
      state.characters = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setComicsPerCharacter (state, payload) {
      state.ComicsPerCharacter = payload
    },
    setFilteredCharacters (state, payload) {
      state.filteredCharacters = payload
    },
    cleanFilteredChracters (state) {
      state.filteredCharacters = []
    },
    setStoriesPerCharacter (state, payload) {
      state.StoriesPerCharacter = payload
    },
    setCharactersPerComic (state, payload) {
      state.setCharactersPerComic = payload
    },
    setComicsPerStory (state, payload) {
      state.comicsPerStory = payload
    },
    setCharactersPerStory (state, payload) {
      state.charactersPerStory = payload
    },
    setStoriesPerComic (state, payload) {
      state.storiesPerComic = payload
    },
    setNoResults (state, payload) {
      state.noResults = payload
    },
    setFavoriteList (state, payload) {
      state.favorites_list = payload
    },
    setFavorite (state, payload) {
      if (payload.list === 'characters') {
        state.favorites_list.characters.push(payload.entry)
      } else if (payload.list === 'comics') {
        state.favorites_list.comics.push(payload.entry)
      } else if (payload.list === 'stories') {
        state.favorites_list.stories.push(payload.entry)
      }
      localStorage.setItem('marvelAppAlexFavorites', JSON.stringify(state.favorites_list))
    },
    removeItemFromFavoriteList (state, payload) {
      if (payload.list === 'characters') {
        state.favorites_list.characters.splice(state.favorites_list.characters.findIndex(el => el.id === payload.entry.id), 1)
      } else if (payload.list === 'comics') {
        state.favorites_list.comics.splice(state.favorites_list.comics.findIndex(el => el.id === payload.entry.id), 1)
      } else if (payload.list === 'stories') {
        state.favorites_list.stories.splice(state.favorites_list.stories.findIndex(el => el.id === payload.entry.id), 1)
      }
      localStorage.setItem('marvelAppAlexFavorites', JSON.stringify(state.favorites_list))
    }
  },
  actions,
  getters: {
    getCharacters (state) {
      return state.characters
    },
    getLoading (state) {
      return state.loading
    },
    getComicsPerCharacter (state) {
      return state.ComicsPerCharacter
    },
    getFilteredCharacters (state) {
      return state.filteredCharacters
    },
    getStoriesPerCharacter (state) {
      return state.StoriesPerCharacter
    },
    getCharactersPerComic (state) {
      return state.setCharactersPerComic
    },
    getComicsPerStory (state) {
      return state.comicsPerStory
    },
    getcharactersPerStory (state) {
      return state.charactersPerStory
    },
    getStoriesPerComic (state) {
      return state.storiesPerComic
    },
    getNoResults (state) {
      return state.noResults
    },
    getFavoriteList (state) {
      return state.favorites_list
    }
  }
})

import AjaxRequests from '../helpers/ajaxRequests.js'
export default {
  loadAllCharacters ({ commit }, payload) {
    commit('setLoading', true)
    console.log(payload)
    let section = 'characters'
    const criteria = `&nameStartsWith=${payload.searchWord}`
    let url = `${process.env.VUE_APP_API_URL}${section}${process.env.VUE_APP_CREDENTIALS}${criteria}`
    if (payload.option === 'comics') {
      section = `comics/${payload.searchId}/characters`
      url = `${process.env.VUE_APP_API_URL}${section}${process.env.VUE_APP_CREDENTIALS}`
    }
    AjaxRequests(url)
      .then(dataCharacters => {
        if (dataCharacters.data.results.length > 0) {
          commit('setCharacters', dataCharacters.data.results)
          commit('setLoading', false)
          commit('cleanFilteredChracters')
        } else {
          commit('setLoading', false)
          commit('setNoResults', true)
        }
      })
      .catch(error => {
        console.log(error)
      })
  },
  filterCharacters ({ commit }, payload) {
    if (payload.searchWord !== '') {
      let section = 'characters'
      let criteria = `&nameStartsWith=${payload.searchWord}`
      if (payload.option === 'comics') {
        section = 'comics'
        criteria = `&titleStartsWith=${payload.searchWord}`
      } else if (payload.option === 'stories') {
        section = 'stories'
        criteria = `&titleStartsWith=${payload.searchWord}`
      }
      const url = `${process.env.VUE_APP_API_URL}${section}${process.env.VUE_APP_CREDENTIALS}${criteria}`
      AjaxRequests(url)
        .then(response => {
          if (response.data.results.length > 0) {
            commit('setFilteredCharacters', response.data.results)
          } else {
            commit('cleanFilteredChracters')
          }
        })
    } else {
      commit('cleanFilteredChracters')
    }
  },
  loadAllComicsPerCharacter ({ commit }, payload) {
    commit('setLoading', true)
    AjaxRequests(`${process.env.VUE_APP_API_URL}/characters/${payload}/comics${process.env.VUE_APP_CREDENTIALS}`)
      .then(dataComics => {
        if (dataComics.data.results.length > 0) {
          commit('setComicsPerCharacter', dataComics.data.results)
          commit('setLoading', false)
        } else {
          commit('setNoResults', true)
          commit('setLoading', false)
        }
      })
  },
  loadAllStoriesPerCharacter ({ commit }, payload) {
    commit('setLoading', true)
    AjaxRequests(`${process.env.VUE_APP_API_URL}/characters/${payload}/stories${process.env.VUE_APP_CREDENTIALS}`)
      .then(dataComics => {
        if (dataComics.data.results.length > 0) {
          commit('setStoriesPerCharacter', dataComics.data.results)
          commit('setLoading', false)
        } else {
          commit('setLoading', false)
          commit('setNoResults', true)
        }
      })
  },
  loadCharactersPerComic ({ commit }, payload) {
    commit('setLoading', true)
    AjaxRequests(`${process.env.VUE_APP_API_URL}/comics/${payload}/characters${process.env.VUE_APP_CREDENTIALS}`)
      .then(response => {
        if (response.data.results.length > 0) {
          commit('setCharactersPerComic', response.data.results)
          commit('setLoading', false)
        } else {
          commit('setLoading', false)
          commit('setNoResults', true)
        }
      })
  },
  loadCharactersPerStory ({ commit }, payload) {
    commit('setLoading', true)
    AjaxRequests(`${process.env.VUE_APP_API_URL}/stories/${payload}/characters${process.env.VUE_APP_CREDENTIALS}`)
      .then(response => {
        if (response.data.results.length > 0) {
          commit('setCharactersPerStory', response.data.results)
          commit('setLoading', false)
        } else {
          commit('setLoading', false)
          commit('setCharactersPerStory', [])
          commit('setNoResults', true)
        }
      })
  },
  loadStoriesPerComic ({ commit }, payload) {
    commit('setLoading', true)
    AjaxRequests(`${process.env.VUE_APP_API_URL}/comics/${payload}/stories${process.env.VUE_APP_CREDENTIALS}`)
      .then(response => {
        if (response.data.results.length > 0) {
          commit('setStoriesPerComic', response.data.results)
          commit('setLoading', false)
        } else {
          commit('setLoading', false)
          commit('setNoResults', true)
        }
      })
  },
  loadComicsPerStory ({ commit }, payload) {
    commit('setLoading', true)
    AjaxRequests(`${process.env.VUE_APP_API_URL}/stories/${payload}/comics${process.env.VUE_APP_CREDENTIALS}`)
      .then(response => {
        if (response.data.results.length > 0) {
          commit('setComicsPerStory', response.data.results)
          commit('setLoading', false)
        } else {
          commit('setLoading', false)
          commit('setNoResults', true)
        }
      })
  },
  clearNoResults ({ commit }) {
    commit('setNoResults', false)
  },
  addToFavoriteList ({ commit }, payload) {
    commit('setFavorite', payload)
  },
  removeItemFromFavoriteList ({ commit }, payload) {
    commit('removeItemFromFavoriteList', payload)
  },
  loadLocalCopyfavoriteList ({ commit }, payload) {
    commit('setFavoriteList', payload)
  }
}

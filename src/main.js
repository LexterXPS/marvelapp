import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

//  Componentes
import loader from '@/components/shared/loader.vue'
import scrollToTopButton from '@/components/shared/scrollToTopButton.vue'
import searchCharacter from '@/components/searchCharacter.vue'
import noResults from '@/components/shared/noResultsFound.vue'

Vue.component('loader', loader)
Vue.component('scrollToTopButton', scrollToTopButton)
Vue.component('searchCharacter', searchCharacter)
Vue.component('noResults', noResults)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

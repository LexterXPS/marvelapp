import Vue from 'vue'
import VueRouter from 'vue-router'
import Characters from '../views/characters.vue'
import Comics from '../views/comics.vue'
import Stories from '../views/stories.vue'
import Favorites from '../views/favorites.vue'
import ComicsPerCharacter from '../views/ComicsPerCharacter.vue'
import StoriesPerCharacter from '../views/storiesPerCharacter.vue'
import ComicDetails from '../views/ComicDetails.vue'
import CharactersPerComic from '../views/charactersPerComic.vue'
import StoriesPerComic from '../views/storiesPerComic.vue'
import ComicsPerStory from '../views/comicsPerStory.vue'
import CharactersPerStory from '../views/charactersPerStory.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Characters',
    component: Characters
  },
  {
    path: '/comics',
    name: 'Comics',
    component: Comics
  },
  {
    path: '/stories',
    name: 'Stories',
    component: Stories
  },
  {
    path: '/favorites',
    name: 'Favorites',
    component: Favorites
  },
  {
    path: '/character/comics/:id_character',
    name: 'ComicsPerCharacter',
    component: ComicsPerCharacter,
    props: true
  },
  {
    path: '/character/stories/:id_character',
    name: 'StoriesPerCharacter',
    component: StoriesPerCharacter,
    props: true
  },
  {
    path: '/character/comics/details/:id_comic',
    name: 'ComicDetail',
    component: ComicDetails,
    props: true
  },
  {
    path: '/character/comics/characters/:id_comic',
    name: 'CharactersPerComic',
    component: CharactersPerComic,
    props: true
  },
  {
    path: '/character/comics/stories/:id_comic',
    name: 'StoriesPerComics',
    component: StoriesPerComic,
    props: true
  },
  {
    path: '/character/stories/characters/:id_story',
    name: 'CharactersPerStory',
    component: CharactersPerStory,
    props: true
  },
  {
    path: '/character/stories/comics/:id_story',
    name: 'ComicsPerStory',
    component: ComicsPerStory,
    props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
